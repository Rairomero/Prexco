﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace Prexco.EntityFramework.Repositories
{
    public abstract class PrexcoRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<PrexcoDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected PrexcoRepositoryBase(IDbContextProvider<PrexcoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class PrexcoRepositoryBase<TEntity> : PrexcoRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected PrexcoRepositoryBase(IDbContextProvider<PrexcoDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
