using Prexco.Models;
using System;
using System.Data.Entity.Migrations;

namespace Prexco.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Prexco.EntityFramework.PrexcoDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "Prexco";
        }

        protected override void Seed(Prexco.EntityFramework.PrexcoDbContext context)
        {
            // This method will be called every time after migrating to the latest version.
            // You can add any seed data here...

            context.Directors.AddOrUpdate(
                d => new
                {
                    d.Name,
                    d.Last_name,
                    d.Birthdate,
                    d.Sex
                }, new Director { Name = "Rai", Last_name = "Romero Chacon", Birthdate = DateTime.ParseExact("1992/12/21", "yyyy/MM/dd", null), Sex = "M" },
                new Director { Name = "Mauricio", Last_name = "Boxtael", Birthdate = DateTime.ParseExact("1992/12/21", "yyyy/MM/dd",null), Sex = "M" },
                new Director { Name = "Andrea", Last_name = "Zambrano", Birthdate = DateTime.ParseExact("1990/03/16", "yyyy/MM/dd", null).Date, Sex = "M" }
                );

            context.SaveChanges();

        }
    }
}
