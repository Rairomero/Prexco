﻿using System.Data.Entity;
using System.Reflection;
using Abp.EntityFramework;
using Abp.Modules;
using Prexco.EntityFramework;

namespace Prexco
{
    [DependsOn(typeof(AbpEntityFrameworkModule), typeof(PrexcoCoreModule))]
    public class PrexcoDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
            Database.SetInitializer<PrexcoDbContext>(null);
        }
    }
}
