﻿using Abp.Application.Services;
using Prexco.Directors.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Directors
{
    public interface IDirectorAppService : IApplicationService
    {
        IEnumerable<GetDirectorOutput> ListAll();
        Task Create(CreateDirectorInput input);
        void Update(UpdateDirectorInput input);
        void Delete(DeleteDirectorInput input);
        GetDirectorOutput GetDirectorById(GetDirectorInput input);
        GetDirectorDetailsOutput GetDirectorMovies(GetDirectorInput input);
    }
}
