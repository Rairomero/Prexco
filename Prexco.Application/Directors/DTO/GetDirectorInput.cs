﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Directors.DTO
{
    public class GetDirectorInput
    {
        public int Id { get; set; }
    }
}
