﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Directors.DTO
{
    public class UpdateDirectorInput
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Last_name { get; set; }

        public DateTime Birthdate { get; set; }

        public string Sex { get; set; }
    }
}
