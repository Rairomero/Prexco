﻿using Prexco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Directors.DTO
{
    public class GetDirectorDetailsOutput
    {
        public GetDirectorDetailsOutput(DirectorDetails details)
        {
            this.Director = details.Director;
            this.MaxCal = details.MaxCal;
            this.MinCal = details.MinCal;
            this.AveCal = details.AveCal;
            this.Movies = details.Movies;
            // y todas las demas aqui
        }

        public int Id { get; set; }
        public Director Director { get; set; }
        public Movie MaxCal { get; set; }
        public Movie MinCal { get; set; }
        public double AveCal { get; set; }
        public List<Movie> Movies { get; set; }
    }
}
