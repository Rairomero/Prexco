﻿using Abp.Application.Services;
using Prexco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prexco.Directors.DTO;
using AutoMapper;

namespace Prexco.Directors
{
    public class DirectorAppService : ApplicationService, IDirectorAppService 
    {
        private readonly IDirectorManager _directorManager;

        public DirectorAppService(IDirectorManager directorManager)
        {
            _directorManager = directorManager;
        }

        public async Task Create(CreateDirectorInput input)
        {
            Director output = Mapper.Map<CreateDirectorInput, Director>(input);
            await _directorManager.Create(output);
        }

        public void Delete(DeleteDirectorInput input)
        {
            _directorManager.Delete(input.Id);
        }

        public GetDirectorOutput GetDirectorById(GetDirectorInput input)
        {
            var getDirector = _directorManager.GetDirectorByID(input.Id);
            GetDirectorOutput output = Mapper.Map<Director, GetDirectorOutput>(getDirector);
            return output;
        }

        public GetDirectorDetailsOutput GetDirectorMovies(GetDirectorInput input)
        {
            var output = _directorManager.GetDirectorMovies(input.Id);

            //GetDirectorOutput output = Mapper.Map<Director, GetDirectorOutput>(getDirector);
            return new GetDirectorDetailsOutput(output);
        }

        public IEnumerable<GetDirectorOutput> ListAll()
        {
            var getAll = _directorManager.GetAllList().ToList();
            List<GetDirectorOutput> output = Mapper.Map<List<Director>, List<GetDirectorOutput>>(getAll);
            return output;
        }

        public void Update(UpdateDirectorInput input)
        {
            Director output = Mapper.Map<UpdateDirectorInput, Director>(input);
            _directorManager.Update(output);
        }
    }
}
