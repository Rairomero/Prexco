﻿using System.Reflection;
using Abp.Modules;
using Abp.AutoMapper;
using Prexco.Directors.DTO;
using Prexco.Models;
using Prexco.Movies.DTO;
using Prexco.Locals.DTO;
using Prexco.Listings.DTO;

namespace Prexco
{
    [DependsOn(typeof(PrexcoCoreModule), typeof(AbpAutoMapperModule))]
    public class PrexcoApplicationModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Modules.AbpAutoMapper().Configurators.Add(mapper => {
                
                #region Directors
                mapper.CreateMap<CreateDirectorInput, Director>().ReverseMap();
                mapper.CreateMap<Director, GetDirectorOutput>().ReverseMap();
                mapper.CreateMap<UpdateDirectorInput, Director>().ReverseMap();
                #endregion

                #region Movies
                mapper.CreateMap<CreateMovieInput, Movie>().ReverseMap();
                mapper.CreateMap<Movie, GetMovieOutput>().ReverseMap();
                mapper.CreateMap<UpdateMovieInput, Movie>().ReverseMap();
                #endregion

                #region Locals
                mapper.CreateMap<CreateLocalInput, Local>().ReverseMap();
                mapper.CreateMap<Local, GetLocalOutput > ().ReverseMap();
                mapper.CreateMap<UpdateLocalInput, Local>().ReverseMap();
                #endregion

                #region Listings
                mapper.CreateMap<CreateListingInput, Listing>().ReverseMap();
                mapper.CreateMap<Listing, GetListingOutput>().ReverseMap();
                mapper.CreateMap<UpdateListingInput, Listing>().ReverseMap();
                #endregion
            });
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
