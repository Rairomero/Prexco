﻿using Abp.Application.Services;
using Prexco.Locals.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Locals
{
    public interface ILocalAppService : IApplicationService
    {
        IEnumerable<GetLocalOutput> ListAll();
        Task Create(CreateLocalInput input);
        void Update(UpdateLocalInput input);
        void Delete(DeleteLocalInput input);
        GetLocalOutput GetDirectorById(GetLocalInput input);
    }
}
