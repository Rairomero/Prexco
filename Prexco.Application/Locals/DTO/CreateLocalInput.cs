﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Locals.DTO
{
    public class CreateLocalInput
    {
        public string Name { get; set; }

        public string City { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
