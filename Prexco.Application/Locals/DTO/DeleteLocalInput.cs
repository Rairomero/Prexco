﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Locals.DTO
{
    public class DeleteLocalInput
    {
        public int Id { get; set; }
    }
}
