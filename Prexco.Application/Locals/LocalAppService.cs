﻿using Abp.Application.Services;
using Prexco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prexco.Locals.DTO;
using AutoMapper;

namespace Prexco.Locals
{
    public class LocalAppService : ApplicationService, ILocalAppService
    {
        private readonly ILocalManager _localManager;

        public LocalAppService(ILocalManager localManager)
        {
            _localManager = localManager;
        }

        public async Task Create(CreateLocalInput input)
        {
            Local output = Mapper.Map<CreateLocalInput, Local>(input);
            await _localManager.Create(output);
        }

        public void Delete(DeleteLocalInput input)
        {
            _localManager.Delete(input.Id);
        }

        public GetLocalOutput GetDirectorById(GetLocalInput input)
        {
            var getLocal = _localManager.GetLocalByID(input.Id);
            GetLocalOutput output = Mapper.Map<Local, GetLocalOutput>(getLocal);
            return output;
        }

        public IEnumerable<GetLocalOutput> ListAll()
        {
            var getAll = _localManager.GetAllList().ToList();
            List<GetLocalOutput> output = Mapper.Map<List<Local>, List<GetLocalOutput>>(getAll);
            return output;
        }

        public void Update(UpdateLocalInput input)
        {
            Local output = Mapper.Map<UpdateLocalInput, Local>(input);
            _localManager.Update(output);
        }
    }
}
