﻿using Abp.Application.Services;
using Prexco.Listings.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Listings
{
    public interface IListingAppService : IApplicationService
    {
        IEnumerable<GetListingOutput> ListAll();
        Task Create(CreateListingInput input);
        void Update(UpdateListingInput input);
        void Delete(DeleteListingInput input);
        GetListingOutput GetListingById(GetListingInput input);
    }
}
