﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Listings.DTO
{
    public class DeleteListingInput
    {
        public int Id { get; set; }
    }
}
