﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Listings.DTO
{
    public class UpdateListingInput
    {
        public int Id { get; set; }

        public DateTime Start { get; set; }

        public int MovieId { get; set; }

        public int LocalId { get; set; }
    }
}
