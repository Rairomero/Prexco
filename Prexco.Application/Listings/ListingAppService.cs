﻿using Abp.Application.Services;
using Prexco.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prexco.Listings.DTO;
using AutoMapper;

namespace Prexco.Listings
{
    public class ListingAppService : ApplicationService, IListingAppService
    {
        private readonly IListingManager _listingManager;
        public ListingAppService(IListingManager listingManager)
        {
            _listingManager = listingManager;
        }

        public async Task Create(CreateListingInput input)
        {
            Listing output = Mapper.Map<CreateListingInput, Listing>(input);
            await _listingManager.Create(output);
        }

        public void Delete(DeleteListingInput input)
        {
            _listingManager.Delete(input.Id);
        }

        public GetListingOutput GetListingById(GetListingInput input)
        {
            var getListing = _listingManager.GetListingByID(input.Id);
            GetListingOutput output = Mapper.Map<Listing, GetListingOutput>(getListing);
            return output;
        }

        public IEnumerable<GetListingOutput> ListAll()
        {
            var getAll = _listingManager.GetAllList().ToList();
            List<GetListingOutput> output = Mapper.Map<List<Listing>, List<GetListingOutput>>(getAll);
            return output;
        }

        public void Update(UpdateListingInput input)
        {
            Listing output = Mapper.Map<UpdateListingInput, Listing>(input);
            _listingManager.Update(output);
        }
    }
}
