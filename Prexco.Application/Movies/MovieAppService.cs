﻿using Abp.Application.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Prexco.Movies.DTO;
using AutoMapper;
using Prexco.Models;

namespace Prexco.Movies
{
    public class MovieAppService : ApplicationService, IMovieAppService
    {
        private readonly IMovieManager _movieManager;

        public MovieAppService(IMovieManager movieManager)
        {
            _movieManager = movieManager;
        }

        public async Task Create(CreateMovieInput input)
        {
            Movie output = Mapper.Map<CreateMovieInput, Movie>(input);
            await _movieManager.Create(output);
        }

        public void Delete(DeleteMovieInput input)
        {
            _movieManager.Delete(input.Id);
        }

        public GetMovieOutput GetMovieById(GetMovieInput input)
        {
            var getMovie = _movieManager.GetMovieByID(input.Id);
            GetMovieOutput output = Mapper.Map<Movie, GetMovieOutput>(getMovie);
            return output;
        }

        public IEnumerable<GetMovieOutput> ListAll()
        {
            var output = _movieManager.GetAllList().Select(x => new GetMovieOutput {
                Id = x.Id,
                Name = x.Name,
                Duration = x.Duration,
                Calification = x.Calification,
                DirectorId = x.DirectorId,
                DirectorName = x.Director.Name,
            }).ToList();
            //List<GetMovieOutput> output = Mapper.Map<List<Movie>, List<GetMovieOutput>>(getAll);
            return output;
        }

        public void Update(UpdateMovieInput input)
        {
            Movie output = Mapper.Map<UpdateMovieInput, Movie>(input);
            _movieManager.Update(output);
        }
    }
}