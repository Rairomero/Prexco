﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Movies.DTO
{
    public class DeleteMovieInput
    {
        public int Id { get; set; }
    }
}
