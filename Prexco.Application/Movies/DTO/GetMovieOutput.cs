﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Movies.DTO
{
    public class GetMovieOutput
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int Duration { get; set; }
        
        public int Calification { get; set; }
        
        public int DirectorId { get; set; }

        public string DirectorName { get; set; }
    }
}
