﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Movies.DTO
{
    public class CreateMovieInput
    {
        public string Name { get; set; }

        public int Duration { get; set; }

        public int Calification { get; set; }

        public int DirectorId { get; set; }

        public DateTime CreationTime { get; set; }
    }
}
