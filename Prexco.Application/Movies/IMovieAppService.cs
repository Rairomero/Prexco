﻿using Abp.Application.Services;
using Prexco.Movies.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Movies
{
    public interface IMovieAppService : IApplicationService
    {
        IEnumerable<GetMovieOutput> ListAll();
        Task Create(CreateMovieInput input);
        void Update(UpdateMovieInput input);
        void Delete(DeleteMovieInput input);
        GetMovieOutput GetMovieById(GetMovieInput input);
    }
}
