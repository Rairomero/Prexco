﻿using Abp.Application.Services;

namespace Prexco
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class PrexcoAppServiceBase : ApplicationService
    {
        protected PrexcoAppServiceBase()
        {
            LocalizationSourceName = PrexcoConsts.LocalizationSourceName;
        }
    }
}