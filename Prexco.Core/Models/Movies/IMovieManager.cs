﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public interface IMovieManager : IDomainService
    {
        IEnumerable<Movie> GetAllList();
        Movie GetMovieByID(int id);
        Task<Movie> Create(Movie entity);
        void Update(Movie entity);
        void Delete(int id);
    }
}
