﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Prexco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class Movie : FullAuditedEntity
    {
        public Movie()
        {
            Listings = new HashSet<Listing>();
        }

        [Required]
        [Display(Name = "Name")]
        [StringLength(64, ErrorMessage ="Tama;o maximo es de 64")]
        public string Name { get; set; }

        public int Duration { get; set; }

        [Range(1, 100, ErrorMessage = "El valor {0} debe estar en un rango del {1} al {2}.")]
        public int Calification { get; set; }

        [ForeignKey("Director")]
        public int DirectorId { get; set; }

        public Director Director { get; set; }

        public ICollection<Listing> Listings { get; set; }
    }
}
