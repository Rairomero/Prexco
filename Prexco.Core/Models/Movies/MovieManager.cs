﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class MovieManager : DomainService, IMovieManager
    {
        private readonly IRepository<Movie> _repositoryMovie;
        public MovieManager(IRepository<Movie> repositoryMovie)
        {
            _repositoryMovie = repositoryMovie;
        }

        public async Task<Movie> Create(Movie entity)
        {
            var movie = _repositoryMovie.FirstOrDefault(x => x.Id == entity.Id);
            if (movie != null)
            {
                throw new UserFriendlyException("Ya existe.");
            }
            else
            {
                return await _repositoryMovie.InsertAsync(entity);
            }
        }

        public void Delete(int id)
        {
            var movie = _repositoryMovie.FirstOrDefault(x => x.Id == id);
            if (movie == null)
            {
                throw new UserFriendlyException("No encontrado.");
            }
            else
            {
                _repositoryMovie.Delete(movie);
            }
        }

        public IEnumerable<Movie> GetAllList()
        {
            return _repositoryMovie.GetAll();
        }

        public Movie GetMovieByID(int id)
        {
            return _repositoryMovie.Get(id);
        }

        public void Update(Movie entity)
        {
            _repositoryMovie.Update(entity);
        }
    }
}
