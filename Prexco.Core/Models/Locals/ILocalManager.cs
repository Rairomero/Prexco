﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public interface ILocalManager : IDomainService
    {
        IEnumerable<Local> GetAllList();
        Local GetLocalByID(int id);
        Task<Local> Create(Local entity);
        void Update(Local entity);
        void Delete(int id);
    }
}
