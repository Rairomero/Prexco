﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class LocalManager : DomainService, ILocalManager
    {
        private readonly IRepository<Local> _repositoryLocal;
        public LocalManager(IRepository<Local> repositoryLocal)
        {
            _repositoryLocal = repositoryLocal;
        }

        public async Task<Local> Create(Local entity)
        {
            var local = _repositoryLocal.FirstOrDefault(x => x.Id == entity.Id);
            if (local != null)
            {
                throw new UserFriendlyException("Ya existe.");
            }
            else
            {
                return await _repositoryLocal.InsertAsync(entity);
            }
        }

        public void Delete(int id)
        {
            var local = _repositoryLocal.FirstOrDefault(x => x.Id == id);
            if (local == null)
            {
                throw new UserFriendlyException("No encontrado.");
            }
            else
            {
                _repositoryLocal.Delete(local);
            }
        }

        public IEnumerable<Local> GetAllList()
        {
            return _repositoryLocal.GetAllIncluding(x => x.Listings);
        }

        public Local GetLocalByID(int id)
        {
            return _repositoryLocal.Get(id);
        }

        public void Update(Local entity)
        {
            _repositoryLocal.Update(entity);
        }
    }
}
