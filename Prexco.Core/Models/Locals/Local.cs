﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class Local : FullAuditedEntity
    {
        public Local()
        {
            Listings = new HashSet<Listing>();
        }

        [Required]
        public string Name { get; set; }

        [Required]
        public string City { get; set; }

        public virtual ICollection<Listing> Listings { get; set; }
    }
}
