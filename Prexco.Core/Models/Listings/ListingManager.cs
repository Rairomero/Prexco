﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class ListingManager : DomainService, IListingManager
    {
        private readonly IRepository<Listing> _repositoryListing;
        public ListingManager(IRepository<Listing> repositoryListing)
        {
            _repositoryListing = repositoryListing;
        }

        public async Task<Listing> Create(Listing entity)
        {
            var listing = _repositoryListing.FirstOrDefault(x => x.Id == entity.Id);
            if (listing != null)
            {
                throw new UserFriendlyException("Ya existe.");
            }
            else
            {
                return await _repositoryListing.InsertAsync(entity);
            }
        }

        public void Delete(int id)
        {
            var listing = _repositoryListing.FirstOrDefault(x => x.Id == id);
            if (listing == null)
            {
                throw new UserFriendlyException("No encontrado.");
            }
            else
            {
                _repositoryListing.Delete(listing);
            }
        }

        public IEnumerable<Listing> GetAllList()
        {
            return _repositoryListing.GetAllIncluding(x => new { x.Movie, x.Local });
        }

        public Listing GetListingByID(int id)
        {
            return _repositoryListing.Get(id);
        }

        public void Update(Listing entity)
        {
            _repositoryListing.Update(entity);
        }
    }
}
