﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public interface IListingManager : IDomainService
    {
        IEnumerable<Listing> GetAllList();
        Listing GetListingByID(int id);
        Task<Listing> Create(Listing entity);
        void Update(Listing entity);
        void Delete(int id);
    }
}
