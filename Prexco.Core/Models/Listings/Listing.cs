﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using Prexco.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class Listing : FullAuditedEntity
    {
        
        [Required]
        public DateTime Start { get; set; } 

        [ForeignKey("Movie")]
        public int MovieId { get; set; }

        public virtual Movie Movie { get; set; }

        [ForeignKey("Local")]
        public int LocalId { get; set; }

        public virtual Local Local { get; set; }
    }
}
