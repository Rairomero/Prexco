﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class DirectorDetails
    {
        public int Id { get; set; }
        public Director Director { get; set; }
        public Movie MaxCal { get; set; }
        public Movie MinCal { get; set; }
        public double AveCal { get; set; }
        public List<Movie> Movies { get; set; }
    }
}
