﻿using Abp.Domain.Repositories;
using Abp.Domain.Services;
using Abp.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class DirectorManager : DomainService, IDirectorManager
    {
        private readonly IRepository<Director> _repositoryDirector;
        private readonly IRepository<Movie> _repositoryMovie;
        public DirectorManager(IRepository<Director> repositoryDirector, IRepository<Movie> repositoryMovie)
        {
            _repositoryDirector = repositoryDirector;
            _repositoryMovie = repositoryMovie;
        }

        public async Task<Director> Create(Director entity)
        {
            var director = _repositoryDirector.FirstOrDefault(x => x.Id == entity.Id);
            if (director != null)
            {
                throw new UserFriendlyException("Ya existe.");
            }
            else
            {
                return await _repositoryDirector.InsertAsync(entity);
            }
        }

        public void Delete(int id)
        {
            var director = _repositoryDirector.FirstOrDefault(x => x.Id == id);
            if (director == null)
            {
                throw new UserFriendlyException("No encontrado.");
            }
            else
            {
                _repositoryDirector.Delete(director);
            }
        }

        public IEnumerable<Director> GetAllList()
        {
            return _repositoryDirector.GetAllIncluding(x => x.Movies);
        }

        public Director GetDirectorByID(int id)
        {
            return _repositoryDirector.Get(id);
        }

        public DirectorDetails GetDirectorMovies(int id)
        {
            int count = _repositoryMovie.GetAll().Where(m => m.DirectorId == id).Count();
            if (count != 0)
            {
                int? max = _repositoryMovie.GetAll().Where(m => m.DirectorId == id).Max(m => m.Calification);
                int? min = _repositoryMovie.GetAll().Where(m => m.DirectorId == id).Min(m => m.Calification);

            
                var DirectorDetails = new DirectorDetails()
                {
                    Director = _repositoryDirector.GetAllIncluding(d => d.Movies).Where(d => d.Id == id).First(),
                    MinCal = _repositoryMovie.GetAll().Where(m => m.Calification == min && m.DirectorId == id).First(),
                    MaxCal = _repositoryMovie.GetAll().Where(m => m.Calification == max && m.DirectorId == id).First(),
                    AveCal = _repositoryMovie.GetAll().Where(m => m.DirectorId == id).Average(m => m.Calification),
                    Movies = _repositoryMovie.GetAll().Where(m => m.DirectorId == id).ToList()
                };

                return DirectorDetails;
            }
            else
            {
                throw new UserFriendlyException("The director does not have movies");
            }
            

            /*return _repositoryDirector.GetAll().Where(d => d.Id == id).Select(f => new DirectorDetails {
                Id = f.Id,
                DirectorName = f.Name,
                MaxCal = _repositoryMovie.
                MinCal = f.Movies.Min(m => m.Calification),
                AveCal = f.Movies.Average(m => m.Calification),
                Movies = f.Movies.Where(m => m.DirectorId == f.Id).ToList()
            }).FirstOrDefault();*/
        }

        public void Update(Director entity)
        {
            if (entity == null)
            {
                throw new UserFriendlyException("Ya existe.");
            }
            else
            {
                _repositoryDirector.Update(entity); ;
            }
            
        }
    }
}
