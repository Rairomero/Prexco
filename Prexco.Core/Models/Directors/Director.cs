﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public class Director : FullAuditedEntity
    {
        public Director()
        {
            Movies = new HashSet<Movie>();
        }

        [Required]
        [Display(Name = "Name")]
        [StringLength(64, ErrorMessage = "El nombre debe ser de menos de 64 caracteres ")]
        public string Name { get; set; }

        [Required]
        public string Last_name { get; set; }

        [Required]
        public DateTime Birthdate { get; set; }

        public string Sex { get; set; }

        public virtual ICollection<Movie> Movies { get; set; }
    }
}
