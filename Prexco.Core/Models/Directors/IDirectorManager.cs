﻿using Abp.Domain.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prexco.Models
{
    public interface IDirectorManager : IDomainService
    {
        IEnumerable<Director> GetAllList();
        Director GetDirectorByID(int id);
        Task<Director> Create(Director entity);
        void Update(Director entity);
        void Delete(int id);
        DirectorDetails GetDirectorMovies(int id);
    }
}
