﻿(function () {
    'use strict';

    var app = angular.module('app', [
        'ngAnimate',
        'ngSanitize',

        'ui.router',
        'ui.bootstrap',
        'ui.jq',

        'abp'
    ]);

    //Configuration for Angular UI routing.
    app.config([
        '$stateProvider', '$urlRouterProvider', '$locationProvider', '$qProvider',
        function ($stateProvider, $urlRouterProvider, $locationProvider, $qProvider) {
            $locationProvider.hashPrefix('');
            $urlRouterProvider.otherwise('/');
            $qProvider.errorOnUnhandledRejections(false);

            $stateProvider
                .state('home', {
                    url: '/',
                    templateUrl: '/App/Main/views/home/home.cshtml',
                    menu: 'Home' //Matches to name of 'Home' menu in PrexcoNavigationProvider
                })
                .state('directors', {
                    url: '/directors',
                    template: '<director-list></director-list>',
                    menu: 'Directors' //Matches to name of 'Director' menu in PrexcoNavigationProvider
                })
                .state('movies', {
                    url: '/movies',
                    template: '<movie-list></movie-list>',
                    menu: 'Movies' //Matches to name of 'Movie' menu in PrexcoNavigationProvider
                })
                .state('locals', {
                    url: '/locals',
                    template: '<local-list></local-list>',
                    menu: 'Locals' //Matches to name of 'Local' menu in PrexcoNavigationProvider
                })
                .state('listings', {
                    url: '/listings',
                    template: '<listing-list></listing-list>',
                    menu: 'Listings' //Matches to name of 'Listing' menu in PrexcoNavigationProvider
                });
        }
    ]);
})();