﻿(function () {
    "use strict"
    var app = angular.module("app");
    app.component("movieList", {
        templateUrl: "~/App/Main/views/movies/movie.cshtml",
        controllerAs: "vm",
        controller: ['abp.services.app.movie', 'abp.services.app.director', controller]
    });
    function controller(MovieService, directorService) {
        var vm = this;

        vm.movies = [];
        vm.movie = {
            name: '',
            duration: '',
            calification: '',
            directorId: ''
        }
        vm.showSubmit = true;
        vm.showEdit = false;

        function getMovies() {
            MovieService.listAll()
                .then(function (res) {
                    vm.movies = res.data;
                });
        }
        getMovies();

        // get Directors
        function getDirectors() {
            directorService.listAll()
                .then(function (res) {
                    vm.directors = res.data;
                });
        }
        getDirectors();

        // Create modal
        vm.openModalforNew = function () {
            $('#myModal').modal('show');
            vm.showSubmit = true;
            vm.showEdit = false;
        }

        // Update modal
        vm.openModalforUpdate = function (data) {
            $('#myModal').modal('show');
            vm.showSubmit = false;
            vm.showEdit = true;
            vm.entity = data;
            vm.movie.id = data.id;
            vm.movie.name = data.name;
            vm.movie.duration = data.duration;
            vm.movie.calification = data.calification;
            vm.movie.directorId = data.directorId.toString();
        }

        //Save movie
        vm.save = function () {
            abp.ui.setBusy();
            MovieService.create(vm.movie)
                .then(function () {
                    abp.notify.info(App.localize('Saved success'));
                    $('#myModal').modal('hide');
                }).finally(function () {
                    abp.ui.clearBusy();
                    getMovies();
                    vm.movie.name = '';
                    vm.movie.duration = '';
                    vm.movie.calification = '';
                    vm.movie.directorId = '';
                });
        }

        // Update movie
        vm.edit = function (data) {
            var x = {};
            x = data;
            x.id = data.movie.id;
            x.name = data.movie.name;
            x.duration = data.movie.duration;
            x.calification = data.movie.calification;
            x.directorId = parseInt(data.movie.directorId);
            abp.ui.setBusy();
            MovieService.update(x)
                .then(function () {
                    abp.notifications.info(App.localize('UpdatedSuccess'));
                    $uibModalInstance.close();
                }).finally(function () {
                    abp.ui.clearBusy();
                    getMovies();
                });
        }

        vm.delete = function (id) {
            var x = { id };
            abp.ui.setBusy();
            MovieService.delete(x)
                .then(function () {
                    abp.notify.info(App.localize('DeleteSuccess'));
                    $('#myModal').modal('hide');
                }).finally(function () {
                    abp.ui.clearBusy();
                    getMovies();
                });
        }
    }
})();