﻿(function () {
    "use strict"
    var app = angular.module("app");
    app.component("directorList", {
        templateUrl: "~/App/Main/views/directors/director.cshtml",
        controllerAs: "vm",
        controller: ['abp.services.app.director', controller]
    });
    function controller(directorService) {
        
        var vm = this;
        vm.isVisible = false;
        vm.details = [];
        vm.directors = [];
        vm.showSubmit = true;
        vm.showEdit = false;
        vm.director = {
            name: '',
            last_name: '',
            birthdate: '',
            sex: ''
        }
        // get Directors
        function getDirectors() {
            directorService.listAll()
                .then(function (res) {
                    vm.directors = res.data;
                });
        }
        getDirectors();
        
        // Create modal
        vm.openModalforNew = function () {
            $('#myModal').modal('show');
            vm.showSubmit = true;
            vm.showEdit = false;
            vm.director.name = '';
            vm.director.last_name = '';
            vm.director.birthdate = '';
            vm.director.sex = '';
        }

        //Update modal
        vm.openModalforUpdate = function (data) {
            $('#myModal').modal('show');
            vm.director.id = data.id;
            vm.director.name = data.name;
            vm.director.last_name = data.last_name;
            vm.director.birthdate = data.birthdate;
            vm.director.sex = data.sex;
            vm.showSubmit = false;
            vm.showEdit = true;
            vm.entity = data;
        }

        // Save button
        vm.save = function () {
            abp.ui.setBusy();
            directorService.create(vm.director)
                .then(function () {
                    abp.notify.info(App.localize('SavedSuccess'));
                    $uibModalInstance.close();
                }).finally(function () {
                    abp.ui.clearBusy();
                    getDirectors();
                });
        }

        vm.edit = function (data) {
            var x = {};
            x = data;
            x.id = data.director.id;
            x.name = data.director.name;
            x.last_name = data.director.last_name;
            x.birthdate = data.director.birthdate;
            x.sex = data.director.sex;
            abp.ui.setBusy();
            directorService.update(x)
                .then(function () {
                    abp.notifications.info(App.localize('UpdatedSuccess'));
                    $uibModalInstance.close();
                }).finally(function () {
                    abp.ui.clearBusy();
                    getDirectors();
                });
        }

        vm.delete = function (id) {
            var x = { id };
            abp.ui.setBusy();
            directorService.delete(x)
                .then(function () {
                    abp.notify.info(App.localize('DeleteSuccess'));
                    $uibModalInstance.close();
                }).finally(function () {
                    abp.ui.clearBusy();
                    getDirectors();
                });
        }

        vm.showDetails = function (data) {
            var x = {};
            console.log(data.director);
            x.id = data.director;
            abp.ui.setBusy();
            directorService.getDirectorMovies(x)
                .then(function (res) {
                    vm.details = res.data;
                }).finally(function (err) {
                    console.log(err);
                    vm.isVisible = true;
                    abp.ui.clearBusy();
                    
                });
        }


    }
})();