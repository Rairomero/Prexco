﻿using Abp.Application.Navigation;
using Abp.Localization;

namespace Prexco.Web
{
    /// <summary>
    /// This class defines menus for the application.
    /// It uses ABP's menu system.
    /// When you add menu items here, they are automatically appear in angular application.
    /// See .cshtml and .js files under App/Main/views/layout/header to know how to render menu.
    /// </summary>
    public class PrexcoNavigationProvider : NavigationProvider
    {
        public override void SetNavigation(INavigationProviderContext context)
        {
            context.Manager.MainMenu
                .AddItem(
                    new MenuItemDefinition(
                        "Home",
                        new LocalizableString("HomePage", PrexcoConsts.LocalizationSourceName),
                        url: "#/",
                        icon: "fa fa-home"
                        )
                ).AddItem(
                    new MenuItemDefinition(
                        "Directors",
                        new LocalizableString("Directors", PrexcoConsts.LocalizationSourceName),
                        url: "#/directors",
                        icon: "fa fa-info"
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Movies",
                        new LocalizableString("Movies", PrexcoConsts.LocalizationSourceName),
                        url: "#/movies",
                        icon: "fa fa-info"
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Locals",
                        new LocalizableString("Locals", PrexcoConsts.LocalizationSourceName),
                        url: "#/locals",
                        icon: "fa fa-info"
                        )
                )
                .AddItem(
                    new MenuItemDefinition(
                        "Listings",
                        new LocalizableString("Listings", PrexcoConsts.LocalizationSourceName),
                        url: "#/listings",
                        icon: "fa fa-info"
                        )
                );
        }
    }
}
